# KNIGHTS TOUR
Author: Noah

Description:
<p>Program to solve knights tour through a brute force approach and heurisitc approach.<p>

## todo:
- [x] write brute force approach
- [ ] write accessiblility heurisitc approach

### instructions:
<p>
The question is this: Can the chess piece called the Knight move around an empty chessboard and touch
each of the 64 squares once and only once? A complete Knight’s Tour is when it moves through all 64
squares once and only once.
The Knight makes L-shaped (over two squares in one direction and then over one square in a
perpendicular direction). Thus, from a square in the middle of an empty chessboard, the Knight (K) can
make eight different moves (numbered 0 through 7) as shown below
0 1 2 3 4 5 6 7
0
1 2 1
2 3 0
3 K
4 4 7
5 5 6
6
7

1) To solve the problem manually, you would draw an 8-by-8 chessboard on a sheet of paper and
attempt a Knight’s Tour by hand. Put a 1 in the first square you move to, a 2 in the second square
you move to next, a 3 in the third, etc. until you can’t make any further moves. How far did you get?
Remember that a complete Knight’s Tour consists of 64 moves.
One solution for a complete Knight’s Tour is shown next

0 1 2 3 4 5 6 7
0 1 64 47 42 15 40 27 34
1 46 43 16 3 28 35 24 39
2 63 2 45 48 41 14 33 26
3 44 17 4 29 36 25 38 23
4 5 62 53 18 49 22 13 32
5 52 59 6 55 30 37 10 21
6 61 54 57 50 19 8 31 12
7 58 51 60 7 56 11 20 9

2) Now develop a program to simulate the manual approach which moves the Knight around a
chessboard. Declare an 8-by-8 array for the board and initialize all locations to zero. Describe each
of the eight possible moves in terms of both their horizontal and vertical directions. For example,
move 0 as shown in the above diagram consists of moving two squares horizontally to the right and
one square vertically up. The eight moves may be described by two parallel (single-subscripted)
arrays, horizontal and vertical as shown next.
int horizontal[8] = {2, 1, -1, -2, -2, -1, 1, 2};

int vertical[8] = {-1, -2, -2, -1, 1, 2, 2, 1};
You’ll have two variables, currentColumn and currentRow, to keep track of the Knight’s current
position. A new move is therefore simply doing
newColumn = currentColumn + horizontal[move];
newRow = currentRow + vertical[move];
where move is a number between 0 and 7 for selecting a move from the vertical and horizontal
arrays. For example, if currentColumn and currentRow is at 4 and 3 respectively, then doing move 0
will move the Knight to column 6 and row 2 as shown in the following calculation
newColumn = 4 + horizontal[0] = 4 + 2 = 6
newRow = 3 + vertical[0] = 3 + -1 = 2
3) Brute-Force Approach.
a) Use a random-number generator to select a random move (0 to 7) from the eight possible
moves to enable the Knight to walk around the chessboard at random. For example, if the
random number is a 3 then the Knight will move -2 in the horizontal direction and move -1 in
the vertical direction as determined from the horizontal and vertical move arrays indexed at 3.
Note that this (-2, -1) move is relative to the Knight’s current location. So if the Knight’s current
location is close to the edge then it is possible that it will go off the board which is not a valid
move. If that happens then you’ll have to find another random move. Also, if the move is to a
square that the Knight has already been to then you don’t want to go there again. The test to
check for a valid move is shown next
if (newRow >= 0 && newRow < 8 && newColumn >= 0 && newColumn < 8 &&
board[newRow][newColumn] == 0)
You might want to break the problem up into smaller sub-problems and implement each of the
sub-problems as a function. For example, you might want to have a function called PrintBoard
that prints out the 8x8 board with all the moves made so far. Another function called NextMove
to find the next valid move. It returns true if a valid move is found and false otherwise.
Keep a Tour counter that counts from 1 to 64 to keep track of how many moves the Knight has
made so far. You can start the Knight at one corner of the board, say location [0][0], so you
assign the Tour counter, which initially has the value 1, into board[0][0]. Each time when
NextMove finds a new move you will increment the Tour counter and assign its value to that
new board location. If the Tour counter reaches 64 then you have found a solution for a
complete tour of the Knight.
b) How far did the Knight get before it got stuck, i.e., can’t make another valid move? In other
words, how far did the Tour counter get to?
c) Repeat the Brute-Force Approach 1000 times. Did the Knight make a complete tour in those
1000 times?

d) Let the program keep repeating until the Knight has made a complete tour, i.e. touch all 64
squares once and only once. How many times did it take? How long (wall clock time) did it take?
Hint: During debugging you might have a lot of cout statements. In order to solve the problem in
a reasonable amount of time, you’ll have to remove as much as possible all the cout statements.
In other words, don’t keep calling the PrintBoard function inside a loop if you have one.
4) Accessibility Heuristic Approach.
(I suggest that you do not do this part until you have successfully implemented the brute-force
approach because this approach is just a minor modification to the brute-force approach.)
a) You may have observed that the outer squares are more troublesome than the squares nearer
the center of the board. In fact, the most troublesome, or inaccessible, squares are the four
corners. Intuition suggests that if you have an option as to where to move to, you should select
the most troublesome squares to move to first and leave open those that are easier to get to.
So that when the board gets filled near the end of the tour, there will be a greater chance of
success.
Develop an “accessibility heuristic” by classifying each of the squares according to how
accessible they are and then always moving the Knight to the square that is most inaccessible.
Use an 8-by-8 array called accessibility containing numbers indicating how many squares each
particular square is accessible. On an empty chessboard, each center square is rated as 8, each
corner square is rated as 2, and the other squares have accessibility numbers of 3, 4, or 6 as
follows:

0 1 2 3 4 5 6 7
0 2 3 4 4 4 4 3 2
1 3 4 6 6 6 6 4 3
2 4 6 8 8 8 8 6 4
3 4 6 8 8 8 8 6 4
4 4 6 8 8 8 8 6 4
5 4 6 8 8 8 8 6 4
6 3 4 6 6 6 6 4 3
7 2 3 4 4 4 4 3 2

b) Write a version of the Knight’s Tour program using the accessibility heuristic. At any time, the
Knight should move to the square with the lowest accessibility number. In case of a tie, the
Knight will randomly select one of the tied squares. The tour begins in any of the four corners.
Did the accessibility heuristic give a better result than the brute-force approach?
c) Another improvement that can be made is that as the Knight moves around the chessboard, the
accessibility numbers is reduced as more and more squares become occupied. So at any given
time during the tour, each square’s accessibility number is equal to the number of squares that
can go to it. You can adjust the accessibility numbers in the accessibility array after each move.
Did this extra improvement on the accessibility heuristic give an even better result?
<p>
