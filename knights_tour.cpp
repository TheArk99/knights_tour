//noah mcmillan

#include "knights_tour.h"

//sets all vars that are relavent back to default
void KnightsTour::clearBoard(void){
    for (int i = 0; i < 8; i++){
      for (int j = 0; j < 8; j++){
        board[i][j] = 0;
      }
    }
    board[0][0] = 1;
    currentColumn = 0;
    currentRow = 0;
    moveKnightIndex = 6;
    newColumn = 0;
    newRow = 0;
    numberOfMovesCompleted = 1;
}

//constructor
KnightsTour::KnightsTour(void){
  clearBoard();
  highestTriedNumberOfMovesCompleted = 0;
  highestTriedNumberOfLooped = 0;
}



//prints the board that is passed in as an arg as to print both the main board and the hightest found board
void KnightsTour::printBoard(int b[8][8]){
    cout << "\t";
  for (int i = 0; i < 8; i++){
    cout << i << "\t";
  }
  cout << endl;
  for (int i = 0; i < 8; i++){
    cout << i;
    cout << "\t";
    for (int j = 0; j < 8; j++){
      cout << b[i][j];
      cout << "\t";
    }
    cout << endl;
  }
  cout << endl;

}


void KnightsTour::moveKnight(void){
  newColumn = currentColumn + HORIZONTAL[moveKnightIndex];
  newRow = currentRow + VERTICAL[moveKnightIndex];
  //checks if the column and row are valid
  if (!(newRow >= 0 && newRow < 8 && newColumn >= 0 && newColumn < 8 && board[newRow][newColumn] == 0)){
    return;
  }
  numberOfMovesCompleted++;
  currentRow = newRow;
  currentColumn = newColumn;
  board[currentRow][currentColumn] = numberOfMovesCompleted;
}


//checks if there are any moves at all possible
bool KnightsTour::movesPossible(void){
  bool foundMove = false;

  for (int i = 0; i < 8; i++){
    int tempColumn = currentColumn + HORIZONTAL[i];
    int tempRow = currentRow + VERTICAL[i];

    if (tempRow >= 0 && tempRow < 8 && tempColumn >= 0 && tempColumn < 8 && board[tempRow][tempColumn] == 0){
      foundMove = true;
      break;
    }

  }

  return foundMove;
}

void KnightsTour::bruteforce(void){
  for (int i = 0; i < 1000000000; i++){

    moveKnight();

    //checks if highest is lower than current
    if (highestTriedNumberOfMovesCompleted < numberOfMovesCompleted){
      highestTriedNumberOfMovesCompleted = numberOfMovesCompleted;

      //copies current board to highest
      //tried using std::copy but seemed not to work, and i am not that familiar it so i will stick to memcpy or memmove if needed
      //memcpy(highestTriedBoard, board, sizeof(board));
      //same thing as memcpy but instead using for loops to copy from board arr
      for (int j = 0; j < 8; j++){
        for (int k = 0; k < 8; k++){
          highestTriedBoard[j][k] = board[j][k];
        }
      }

      //number of loops to get to this highest
      highestTriedNumberOfLooped = i;
    }

    //checks if there are not any possible moves to make
    if (!movesPossible()){

      //checks if it found all possible moves
      if (numberOfMovesCompleted == 64){
        break;
      //else it clears board and starts again
      }else{
        clearBoard();
      }

    }

    moveKnightIndex = rand() % 8;

  }

}


void KnightsTour::oneTour(void){
  for (int i = 0; i < 1000000000; i++){

    moveKnight();

    //checks if highest is lower than current
    if (highestTriedNumberOfMovesCompleted < numberOfMovesCompleted){
      highestTriedNumberOfMovesCompleted = numberOfMovesCompleted;

      //copies current board to highest
      //tried using std::copy but seemed not to work, and i am not that familiar it so i will stick to memcpy or memmove if needed
      //memcpy(highestTriedBoard, board, sizeof(board));
      //same thing as memcpy but instead using for loops to copy from board arr
      for (int j = 0; j < 8; j++){
        for (int k = 0; k < 8; k++){
          highestTriedBoard[j][k] = board[j][k];
        }
      }

      //number of loops to get to this highest
      highestTriedNumberOfLooped = i;
    }

    //checks if there are not any possible moves to make
    if (!movesPossible()){

      //checks if it found all possible moves
      if (numberOfMovesCompleted == 64){
        break;
      //else it clears board and starts again
      }else{
        break;
      }

    }

    moveKnightIndex = rand() % 8;

  }

}


void KnightsTour::accessibility(void){
}


void KnightsTour::reduceAccessibility(void){
}


void KnightsTour::tour(int type){
  srand(time(nullptr));

  switch(type){
    case 1:
      oneTour();
      break;
    case 2:
      bruteforce();
      break;
    case 3:
      accessibility();
      break;
    case 4:
      reduceAccessibility();
      break;
  }

  printBoard(highestTriedBoard);
  cout << "Highest move made: " << highestTriedNumberOfMovesCompleted << " at " << highestTriedNumberOfLooped << " tried times" << endl;

}
