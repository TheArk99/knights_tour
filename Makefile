CXX := g++
CXXFlags := -Wall -g -pipe -O2 -march=native -D_GLIBCXX_ASSERTIONS -std=c++20

.PHONY: all clean obj main

all: main

obj:
	$(CXX) -c knights_tour.cpp -o bin/knights_tour.o $(CXXFlags)

main: obj
	$(CXX) -o bin/main main.cpp bin/knights_tour.o  $(CXXFlags)

install: clean
	mkdir -p bin/
	$(MAKE) all


clean:
	rm -rf bin/
