//noah mcmillan

#ifndef __KNIGHTs_TOUR__
#define __KNIGHTs_TOUR__

#include <iostream>
#include <string>
#include <cstdlib>
//#include <unistd.h>
#include <cstring>
#include <stdio.h>
#include <ctime>
#include <mutex>
#include <thread>
//#include <algorithm>
//#include <array>
using namespace std;

class KnightsTour{
private:
  int board[8][8];
  const int HORIZONTAL[8] = {2, 1, -1, -2, -2, -1, 1, 2};
  const int VERTICAL[8] = {-1, -2, -2, -1, 1, 2, 2, 1};
  int currentColumn;
  int currentRow;
  int moveKnightIndex;
  int newColumn;
  int newRow;
  int numberOfMovesCompleted;
  int highestTriedBoard[8][8];
  int highestTriedNumberOfMovesCompleted;
  int highestTriedNumberOfLooped;
public:
//constructor
KnightsTour(void);

//funcs
  void printBoard(int board[8][8]);
  void clearBoard(void);
  void moveKnight(void);
  bool movesPossible(void);
  void tour(int type);
  void oneTour(void);
  void bruteforce(void);
  void accessibility(void);
  void reduceAccessibility(void);
};

#endif

