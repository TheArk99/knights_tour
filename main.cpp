//noah mcmillan


#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include "knights_tour.h"
using namespace std;

int main(int argc, char** argv){
  int type;

  if (argc >= 2){
    type = atoi(argv[1]);
  }else{
    cout << "Select method: " << endl;
    cout << "1 = One tour" << endl;
    cout << "2 = Brute force" << endl;
    cout << "3 = Accessibility" << endl;
    cout << "4 = Reduce accessibility" << endl;
    cout << "# ";
    cin >> type;
    cout << endl;
  }


  KnightsTour knightsTour;

  knightsTour.tour(type);

  return 0;
}
